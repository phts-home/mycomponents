unit CountBtn;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls, ExtCtrls;


type
  TShowTp = (Normal, CountToCaption);
  TCountBtn = class(TButton)
  private
    FCount: integer;
    FShowType: TShowTp;
  protected
    procedure Click; override;
  public
    procedure ShowCount;
  published
    constructor Create (aowner:TComponent); override;
    property Count: integer read FCount write FCount;
    property ShowType: TshowTp read FshowType write FShowType;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Samples', [TCountBtn]);
end;

constructor TCountBtn.Create(aowner:TComponent);
begin
inherited create(aowner);
end;

procedure TCountBtn.Click;
begin
inherited click;
FCount:=FCount + 1;
if ShowType=Normal then
  Caption:=Caption;
if ShowType=CountToCaption then
  Caption:='Count = '+inttostr(count);
end;

procedure TCountBtn.ShowCount;
begin
ShowMessage('�� ������ '+ caption+' �� �������: '+inttostr(FCount)+' �����(��)');
end;

end.

