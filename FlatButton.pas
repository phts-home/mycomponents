{**************************************************}
{             --- FlatButton v.1.0 ---             }
{  -  Created by Phil Tsarik                       }
{  -  E-mail:  philip-s@yandex.ru                  }
{  -  Site:    http://philip-s.narod.ru            }
{**************************************************}

unit FlatButton;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls;

type
  TBtnType = (Border01, Border02, Bevel, Flat);

  TFlatButton = class(TPanel)
  private
    FColorNormalState,FColorHighLight,FColorPushedState, FColorDisabled,
    FFontColorNormalState, FFontColorHighLight, FFontColorPushedState, FFontColorDisabled: TColor;
    FColorHighLightEnable, FMoveOnClick, FHold, FCanGo, FDownHighLight, FDown, FEnableButton: Boolean;
  protected
    FBtnType : TBtnType;
    procedure SetBtnType (Value: TBtnType);
    procedure SetColorNormalState (Value: TColor);
    procedure SetColorHighLight (Value: TColor);
    procedure SetColorPushedState (Value: TColor);
    procedure SetColorDisabled (Value: TColor);
    procedure SetFontColorNormalState (Value: TColor);
    procedure SetFontColorHighLight (Value: TColor);
    procedure SetFontColorPushedState (Value: TColor);
    procedure SetFontColorDisabled (Value: TColor);
    procedure SetHold (Value: boolean);
    procedure SetDown (Value: boolean);
    procedure SetEnableButton (Value: boolean);
    procedure MouseOver;
    procedure MouseUnOver;
    procedure MouseDown (Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
        override;
    procedure MouseUp (Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
        override;
    procedure MouseMove (Shift: TShiftState; X, Y: Integer); override;
    procedure WndProc(var message : TMessage); override;
  public

  published
    constructor Create (aowner:TComponent); override;
    property ColorNormalState: TColor read FColorNormalState
        write SetColorNormalState default clBtnface;
    property ColorHighLight: TColor read FColorHighLight write SetColorHighLight
        default clWhite;
    property ColorPushedState: TColor read FColorPushedState
        write SetColorPushedState default clBtnHighlight;
    property ColorDisabled: TColor read FColorDisabled write SetColorDisabled
        default clBtnHighlight;
    property ButtonType: TBtnType read FBtnType write SetBtnType default Bevel;
    property ColorHighLightEnable: Boolean read FColorHighLightEnable
        write FColorHighLightEnable default False;
    property MoveOnClick: Boolean read FMoveOnClick write FMoveOnClick
        default True;
    property FontColorNormalState: TColor read FFontColorNormalState
        write SetFontColorNormalState default clWindowText;
    property FontColorHighLight: TColor read FFontColorHighLight
        write SetFontColorHighLight default clWindowText;
    property FontColorPushedState: TColor read FFontColorPushedState
        write SetFontColorPushedState default clWindowText;
    property FontColorDisabled: TColor read FFontColorDisabled
        write SetFontColorDisabled default clInactiveCaptionText;
    property DownAndHold: Boolean read FHold write SetHold default False;
    property DownHighLight: Boolean read FDownHighLight write FDownHighLight
        default False;
    property Down: Boolean read FDown write SetDown default False;
    property EnableButton: Boolean read FEnableButton write SetEnableButton
        default True;
  end;

procedure Register;

implementation

procedure Register;
begin
RegisterComponents('Samples', [TFlatButton]);
end;

constructor TFlatButton.Create (aowner: TComponent);
begin
inherited create (aowner);
Width:=75;
Height:=25;
SetColorNormalState (clBtnFace);
SetColorHighLight (clWhite);
SetColorPushedState (clBtnHighlight);
SetColorDisabled (clBtnHighlight);
SetFontColorNormalState (clWindowText);
SetFontColorHighLight (clWindowText);
SetFontColorPushedState (clWindowText);
SetFontColorDisabled (clInactiveCaptionText);
SetDown (false);
SetEnableButton (true);
FColorHighLightEnable:=false;
FDownHighLight:=false;
FMoveOnClick:=true;
SetBtnType (Bevel);
end;

procedure TFlatButton.MouseDown(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
begin
inherited;
if Button=mbLeft then
  begin
  Color:=FColorPushedState;
  Font.Color:=FFontColorPushedState;
  case FBtnType of
    Border01:
      begin
      BevelInner:= bvNone;
      BevelOuter:= bvNone;
      Ctl3D:= False;
      BorderStyle:= bsSingle;
      end;
    Border02:
      begin
      BevelInner:=bvLowered;
      BevelOuter:=bvLowered;
      Ctl3D:= True;
      BorderStyle:= bsNone;
      end;
    Bevel:
      begin
      BevelInner:=bvNone;
      BevelOuter:=bvLowered;
      Ctl3D:= True;
      BorderStyle:= bsNone;
      end;
    Flat:
      begin
      BevelInner:= bvNone;
      BevelOuter:= bvNone;
      Ctl3D:= True;
      BorderStyle:= bsNone;
      end;
    end;
  if FMoveOnClick and not Down then
    begin
    Left:=Left+1;
    Top:=Top+1;
    end;
  if FHold and not Down then FCanGo:=false;
  FDown:=true;
  end;
end;

procedure TFlatButton.MouseUp(Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
inherited;
if Button=mbLeft then
  begin
  if not FHold or FCanGo then
    begin
    Color:=FColorNormalState;
    Font.Color:=FFontColorNormalState;
    case FBtnType of
      Border01:
        begin
        BevelInner:= bvNone;
        BevelOuter:= bvNone;
        Ctl3D:= False;
        BorderStyle:= bsSingle;
        end;
      Border02:
        begin
        BevelInner:= bvLowered;
        BevelOuter:= bvRaised;
        Ctl3D:= True;
        BorderStyle:= bsNone;
        end;
      Bevel:
        begin
        BevelInner:= bvNone;
        BevelOuter:= bvRaised;
        Ctl3D:= True;
        BorderStyle:= bsNone;
        end;
      Flat:
        begin
        BevelInner:= bvNone;
        BevelOuter:= bvNone;
        Ctl3D:= True;
        BorderStyle:= bsNone;
        end;
      end;
    if FMoveOnClick then
      begin
      Left:=Left-1;
      Top:=Top-1;
      end;
    FDown:=false;
    end;
  FCanGo:=true;
  end;
end;

procedure TFlatButton.MouseMove(Shift: TShiftState; X, Y: Integer);
begin
inherited;
if (Shift<>[ssLeft])and(Shift<>[ssRight])and(Shift<>[ssMiddle]) then
  begin
  MouseOver;
  end
end;

procedure TFlatButton.MouseOver;
begin
if (not Down or FDownHighLight)and FColorHighLightEnable then
  begin
  Color:=FColorHighLight;
  Font.Color:=FFontColorHighLight;
  end;
end;

procedure TFlatButton.MouseUnOver;
begin
if not Down then
  begin
  Color:=FColorNormalState;
  Font.Color:=FFontColorNormalState;
  end else
  begin
  Color:=FColorPushedState;
  Font.Color:=FFontColorPushedState;
  end;
end;

procedure TFlatButton.WndProc(var message: TMessage);
var P1,P2 : TPoint;
begin
if (Parent <> nil) and FEnableButton then
  begin
  GetCursorPos(P1);
  P2 := Self.ScreenToClient(P1);
  if not((P2.X > 0) and (P2.X < Width-1) and (P2.Y > 0) and (P2.Y < Height-1))
    then MouseUnOver;
  end;
inherited WndProc(message);
end;

procedure TFlatButton.SetBtnType (Value: TBtnType);
begin
Color:=FColorNormalState;
Font.Color:=FFontColorNormalState;
FBtnType:= Value;
case Value of
  Border01:
    begin
    BevelInner:= bvNone;
    BevelOuter:= bvNone;
    Ctl3D:= False;
    BorderStyle:= bsSingle;
    end;
  Border02:
    begin
    BevelInner:= bvLowered;
    BevelOuter:= bvRaised;
    Ctl3D:= True;
    BorderStyle:= bsNone;
    end;
  Bevel:
    begin
    BevelInner:= bvNone;
    BevelOuter:= bvRaised;
    Ctl3D:= True;
    BorderStyle:= bsNone;
    end;
  Flat:
    begin
    BevelInner:= bvNone;
    BevelOuter:= bvNone;
    Ctl3D:= True;
    BorderStyle:= bsNone;
    end;
  end;
end;

procedure TFlatButton.SetColorHighLight(Value: TColor);
begin
FColorHighLight:=Value;
end;

procedure TFlatButton.SetColorNormalState(Value: TColor);
begin
FColorNormalState:=Value;
Color:=FColorNormalState;
end;

procedure TFlatButton.SetColorPushedState(Value: TColor);
begin
FColorPushedState:=Value;
end;

procedure TFlatButton.SetColorDisabled(Value: TColor);
begin
FColorDisabled:=Value;
end;

procedure TFlatButton.SetFontColorHighLight(Value: TColor);
begin
FFontColorHighLight:=Value;
end;

procedure TFlatButton.SetFontColorNormalState(Value: TColor);
begin
FFontColorNormalState:=Value;
Font.Color:=FFontColorNormalState;
end;

procedure TFlatButton.SetFontColorPushedState(Value: TColor);
begin
FFontColorPushedState:=Value;
end;

procedure TFlatButton.SetFontColorDisabled(Value: TColor);
begin
FFontColorDisabled:=Value;
end;

procedure TFlatButton.SetHold (Value: boolean);
begin
if not value then
  begin
  SetDown(false);
  end;
FHold:=Value;
end;

procedure TFlatButton.SetDown(Value: boolean);
var OldVal: boolean;
begin
if FHold then
  begin

Oldval:=FDown;
FDown:=Value;

if value then

  begin
  Color:=FColorPushedState;
  Font.Color:=FFontColorPushedState;
  case FBtnType of
    Border01:
      begin
      BevelInner:= bvNone;
      BevelOuter:= bvNone;
      Ctl3D:= False;
      BorderStyle:= bsSingle;
      end;
    Border02:
      begin
      BevelInner:=bvLowered;
      BevelOuter:=bvLowered;
      Ctl3D:= True;
      BorderStyle:= bsNone;
      end;
    Bevel:
      begin
      BevelInner:=bvNone;
      BevelOuter:=bvLowered;
      Ctl3D:= True;
      BorderStyle:= bsNone;
      end;
    Flat:
      begin
      BevelInner:= bvNone;
      BevelOuter:= bvNone;
      Ctl3D:= True;
      BorderStyle:= bsNone;
      end;
    end;
  if FMoveOnClick and OldVal<>FDown then
    begin
    Left:=Left+1;
    Top:=Top+1;
    end;
  FCanGo:=true;
  end else
  begin

  Color:=FColorNormalState;
  Font.Color:=FFontColorNormalState;
  case FBtnType of
    Border01:
      begin
      BevelInner:= bvNone;
      BevelOuter:= bvNone;
      Ctl3D:= False;
      BorderStyle:= bsSingle;
      end;
    Border02:
      begin
      BevelInner:= bvLowered;
      BevelOuter:= bvRaised;
      Ctl3D:= True;
      BorderStyle:= bsNone;
      end;
    Bevel:
      begin
      BevelInner:= bvNone;
      BevelOuter:= bvRaised;
      Ctl3D:= True;
      BorderStyle:= bsNone;
      end;
    Flat:
      begin
      BevelInner:= bvNone;
      BevelOuter:= bvNone;
      Ctl3D:= True;
      BorderStyle:= bsNone;
      end;
    end;
  if FMoveOnClick and OldVal<>FDown then
    begin
    Left:=Left-1;
    Top:=Top-1;
    end;
  end;

  end;
end;

procedure TFlatButton.SetEnableButton(Value: boolean);
begin
FEnableButton:=Value;
Enabled:=Value;
if Value then
  begin
  Color:=FColorNormalState;
  Font.Color:=FFontColorNormalState;
  end else
  begin
  Color:=FColorDisabled;
  Font.Color:=FFontColorDisabled;
  end;
end;


end.
