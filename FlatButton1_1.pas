{**********************************************************************}
{                      --- FlatButton v 1.1b ---                        }
{  -  Created by Phil Tsarik                                           }
{  -  E-mail:  philip-s@yandex.ru                                      }
{  -  Site:    http://philip-s.narod.ru                                }
{
{ ��������.
{ ������ ����� ������ ����(Bevel, Border01, Border02, Flat) � ������
{ ��������� (Normal, Highlight, Pushed, Disabled). �� ������ ���������
{ ����� �������� ���� ���� ������ � ���� ������.
{
{ ��������:
{ - ButtonType - ����� ���� ������
{ - ColorDisabled - ���� ������ � ��������� EnableButton = False
{ - ColorHighLight - ���� ������ ��� ��������� �� ��� ������
{                      ��� ColorHighLight=True
{ - ColorHighLightEnable - ��������� �����. ������
{ - ColorNormalState - ���� ������ � ���������� ���������
{ - ColorPushedState - ���� ������ � ������� ���������
{ - Down -
{ - DownAndHold -
{ - DownHighLight -
{ - EnableButton -
{ - FontColorDisabled -
{ - FontColorHighLight -
{ - FontColorNormalState -
{ - FontColorPushedState -
{
{**********************************************************************}

unit FlatButton;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls;

type
  TBtnType = (Border01, Border02, Bevel, Flat);
{
TFont = class(TGraphicsObject)
  private
    FColor: TColor;
    FPixelsPerInch: Integer;
    FNotify: IChangeNotifier;
    procedure GetData(var FontData: TFontData);
    procedure SetData(const FontData: TFontData);
  protected
    procedure Changed; override;
    function GetHandle: HFont;
    function GetHeight: Integer;
    function GetName: TFontName;
    function GetPitch: TFontPitch;
    function GetSize: Integer;
    function GetStyle: TFontStyles;
    function GetCharset: TFontCharset;
    procedure SetColor(Value: TColor);
    procedure SetHandle(Value: HFont);
    procedure SetHeight(Value: Integer);
    procedure SetName(const Value: TFontName);
    procedure SetPitch(Value: TFontPitch);
    procedure SetSize(Value: Integer);
    procedure SetStyle(Value: TFontStyles);
    procedure SetCharset(Value: TFontCharset);
  public
    constructor Create;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    property FontAdapter: IChangeNotifier read FNotify write FNotify;
    property Handle: HFont read GetHandle write SetHandle;
    property PixelsPerInch: Integer read FPixelsPerInch write FPixelsPerInch;
  published
    property Charset: TFontCharset read GetCharset write SetCharset;
    property Color: TColor read FColor write SetColor;
    property Height: Integer read GetHeight write SetHeight;
    property Name: TFontName read GetName write SetName;
    property Pitch: TFontPitch read GetPitch write SetPitch default fpDefault;
    property Size: Integer read GetSize write SetSize stored False;
    property Style: TFontStyles read GetStyle write SetStyle;
  end;     }

//  TFontStyle = (fsBold, fsItalic, fsUnderline, fsStrikeOut);
//  TFontStyles = set of TFontStyle;

  TFormalFont = class(TGraphicsObject)
  private
    FFCharset: TFontCharset;
    FFColor: TColor;
    FFHeight: Integer;
    FFName: TFontName;
    FFPitch: TFontPitch;
    FFSize: Integer;
    FFStyle: TFontStyles;
//    FColor: TColor;
{    FPixelsPerInch: Integer;
    FNotify: IChangeNotifier;
    procedure GetData(var FontData: TFontData);
    procedure SetData(const FontData: TFontData);   }
  protected
//    procedure Changed; override;
{    function GetHandle: HFont;
    function GetHeight: Integer;
    function GetName: TFontName;
    function GetPitch: TFontPitch;
    function GetSize: Integer;
    function GetStyle: TFontStyles;
    function GetCharset: TFontCharset;
    procedure SetColor(Value: TColor);
    procedure SetHandle(Value: HFont);
    procedure SetHeight(Value: Integer);
    procedure SetName(const Value: TFontName);
    procedure SetPitch(Value: TFontPitch);
    procedure SetSize(Value: Integer);
    procedure SetStyle(Value: TFontStyles);
    procedure SetCharset(Value: TFontCharset);   }
  public
{    constructor Create;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    property FontAdapter: IChangeNotifier read FNotify write FNotify;
    property Handle: HFont read GetHandle write SetHandle;
    property PixelsPerInch: Integer read FPixelsPerInch write FPixelsPerInch; }
  published
    property Charset: TFontCharset read FFCharset write FFCharset;
    property Color: TColor read FFColor write FFColor;
    property Height: Integer read FFHeight write FFHeight;
    property Name: TFontName read FFName write FFName;
    property Pitch: TFontPitch read FFPitch write FFPitch default fpDefault;
    property Size: Integer read FFSize write FFSize stored False;
    property Style: TFontStyles read FFStyle write FFStyle;
  end;

  TFlatButton = class(TPanel)
  private
    FColorNormalState,FColorHighLight,FColorPushedState, FColorDisabled: TColor;
//    FFontColorNormalState, FFontColorHighLight, FFontColorPushedState, FFontColorDisabled: TColor;
    FColorHighLightEnable, FMoveOnClick, FHold, FCanGo, FDownHighLight, FDown, FEnableButton: Boolean;
    FFontDisabled: TFormalFont;
    FFontHighLight: TFormalFont;
    FFontNormalState: TFont;
    FFontPushedState: TFormalFont;
  protected
    FBtnType: TBtnType;
{    FFontDisabled: TFont;
    FFontHighLight: TFont;
    FFontNormalState: TFont;
    FFontPushedState: TFont;}
    procedure SetBtnType (Value: TBtnType);
    procedure SetColorNormalState (Value: TColor);
    procedure SetColorHighLight (Value: TColor);
    procedure SetColorPushedState (Value: TColor);
    procedure SetColorDisabled (Value: TColor);
    procedure SetFontNormalState (Value: TFont);
    procedure SetFontHighLight (Value: TFormalFont);
    procedure SetFontPushedState (Value: TFormalFont);
    procedure SetFontDisabled (Value: TFormalFont);
    procedure SetHold (Value: Boolean);
    procedure SetDown (Value: Boolean);
    procedure SetEnableButton (Value: Boolean);
    procedure MouseOver;
    procedure MouseUnOver;
    procedure MouseDown (Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
        override;
    procedure MouseUp (Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
        override;
    procedure MouseMove (Shift: TShiftState; X, Y: Integer); override;
    procedure WndProc(var message : TMessage); override;
  public

  published
    constructor Create (aowner:TComponent); override;
    property ColorNormalState: TColor read FColorNormalState
        write SetColorNormalState default clBtnface;
    property ColorHighLight: TColor read FColorHighLight write SetColorHighLight
        default clWhite;
    property ColorPushedState: TColor read FColorPushedState
        write SetColorPushedState default clBtnHighlight;
    property ColorDisabled: TColor read FColorDisabled write SetColorDisabled
        default clBtnHighlight;
    property ButtonType: TBtnType read FBtnType write SetBtnType default Bevel;
    property ColorHighLightEnable: Boolean read FColorHighLightEnable
        write FColorHighLightEnable default False;
    property MoveOnClick: Boolean read FMoveOnClick write FMoveOnClick
        default True;
{    property FontColorNormalState: TColor read FFontColorNormalState
        write SetFontColorNormalState default clWindowText;
    property FontColorHighLight: TColor read FFontColorHighLight
        write SetFontColorHighLight default clWindowText;
    property FontColorPushedState: TColor read FFontColorPushedState
        write SetFontColorPushedState default clWindowText;
    property FontColorDisabled: TColor read FFontColorDisabled
        write SetFontColorDisabled default clInactiveCaptionText;   }
    property DownAndHold: Boolean read FHold write SetHold default False;
    property DownHighLight: Boolean read FDownHighLight write FDownHighLight
        default False;
    property Down: Boolean read FDown write SetDown default False;
    property EnableButton: Boolean read FEnableButton write SetEnableButton
        default True;
    property FontDisabled: TFormalFont read FFontDisabled write SetFontDisabled;
    property FontHighLight: TFormalFont read FFontHighLight write SetFontHighLight;
    property FontNormalState: TFont read FFontNormalState write SetFontNormalState;
    property FontPushedState: TFormalFont read FFontPushedState write SetFontPushedState;
  end;

procedure Register;

implementation

procedure Register;
begin
RegisterComponents('Samples', [TFlatButton]);
end;

constructor TFlatButton.Create (aowner: TComponent);
begin
inherited create (AOwner);
Width:=75;
Height:=25;
SetColorNormalState (clBtnFace);
SetColorHighLight (clWhite);
SetColorPushedState (clBtnHighlight);
SetColorDisabled (clBtnHighlight);

SetFontNormalState(Font);

FFontDisabled.Charset:=FFontNormalState.Charset;
FFontDisabled.Color:=FFontNormalState.Color;
FFontDisabled.Height:=FFontNormalState.Height;
FFontDisabled.Name:=FFontNormalState.Name;
FFontDisabled.Pitch:=FFontNormalState.Pitch;
FFontDisabled.Size:=FFontNormalState.Size;
FFontDisabled.Style:=FFontNormalState.Style;

SetFontDisabled(FFontDisabled);
SetFontHighLight(FFontDisabled);
//SetFontNormalState(Font);
SetFontPushedState(FFontDisabled);
{SetFontColorNormalState (clWindowText);
SetFontColorHighLight (clWindowText);
SetFontColorPushedState (clWindowText);
SetFontColorDisabled (clInactiveCaptionText);  }
SetDown(False);
SetEnableButton(True);
FColorHighLightEnable:=False;
FDownHighLight:=False;
FMoveOnClick:=True;
SetBtnType(Bevel);
end;

procedure TFlatButton.MouseDown(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
begin
inherited;
if Button=mbLeft then
  begin
  Font.Charset:=FFontPushedState.Charset;
  Font.Color:=FFontPushedState.Color;
  Font.Height:=FFontPushedState.Height;
  Font.Name:=FFontPushedState.Name;
  Font.Pitch:=FFontPushedState.Pitch;
  Font.Size:=FFontPushedState.Size;
  Font.Style:=FFontPushedState.Style;

  Color:=FColorPushedState;
//  Font.Color:=FFontColorPushedState;
//  Font:=FFontPushedState;
  case FBtnType of
    Border01:
      begin
      BevelInner:= bvNone;
      BevelOuter:= bvNone;
      Ctl3D:= False;
      BorderStyle:= bsSingle;
      end;
    Border02:
      begin
      BevelInner:=bvLowered;
      BevelOuter:=bvLowered;
      Ctl3D:= True;
      BorderStyle:= bsNone;
      end;
    Bevel:
      begin
      BevelInner:=bvNone;
      BevelOuter:=bvLowered;
      Ctl3D:= True;
      BorderStyle:= bsNone;
      end;
    Flat:
      begin
      BevelInner:= bvNone;
      BevelOuter:= bvNone;
      Ctl3D:= True;
      BorderStyle:= bsNone;
      end;
    end;
  if FMoveOnClick and not Down then
    begin
    Left:=Left+1;
    Top:=Top+1;
    end;
  if FHold and not Down then FCanGo:=false;
  FDown:=true;
  end;
end;

procedure TFlatButton.MouseUp(Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
inherited;
if Button=mbLeft then
  begin
  if not FHold or FCanGo then
    begin
    Color:=FColorNormalState;
    Font:=FFontNormalState;
//    Font.Color:=FFontColorNormalState;
    case FBtnType of
      Border01:
        begin
        BevelInner:= bvNone;
        BevelOuter:= bvNone;
        Ctl3D:= False;
        BorderStyle:= bsSingle;
        end;
      Border02:
        begin
        BevelInner:= bvLowered;
        BevelOuter:= bvRaised;
        Ctl3D:= True;
        BorderStyle:= bsNone;
        end;
      Bevel:
        begin
        BevelInner:= bvNone;
        BevelOuter:= bvRaised;
        Ctl3D:= True;
        BorderStyle:= bsNone;
        end;
      Flat:
        begin
        BevelInner:= bvNone;
        BevelOuter:= bvNone;
        Ctl3D:= True;
        BorderStyle:= bsNone;
        end;
      end;
    if FMoveOnClick then
      begin
      Left:=Left-1;
      Top:=Top-1;
      end;
    FDown:=false;
    end;
  FCanGo:=true;
  end;
end;

procedure TFlatButton.MouseMove(Shift: TShiftState; X, Y: Integer);
begin
inherited;
if (Shift<>[ssLeft])and(Shift<>[ssRight])and(Shift<>[ssMiddle]) then
  begin
  MouseOver;
  end
end;

procedure TFlatButton.MouseOver;
begin
if (not Down or FDownHighLight)and FColorHighLightEnable then
  begin
  Color:=FColorHighLight;
  Font.Charset:=FFontHighLight.Charset;
  Font.Color:=FFontHighLight.Color;
  Font.Height:=FFontHighLight.Height;
  Font.Name:=FFontHighLight.Name;
  Font.Pitch:=FFontHighLight.Pitch;
  Font.Size:=FFontHighLight.Size;
  Font.Style:=FFontHighLight.Style;
//  Font:=FFontHighLight;
//  Font.Color:=FFontColorHighLight;
  end;
end;

procedure TFlatButton.MouseUnOver;
begin
if not Down then
  begin
  Color:=FColorNormalState;
  Font:=FFontNormalState;
//  Font.Color:=FFontColorNormalState;
  end else
  begin
  Color:=FColorPushedState;
  Font.Charset:=FFontPushedState.Charset;
  Font.Color:=FFontPushedState.Color;
  Font.Height:=FFontPushedState.Height;
  Font.Name:=FFontPushedState.Name;
  Font.Pitch:=FFontPushedState.Pitch;
  Font.Size:=FFontPushedState.Size;
  Font.Style:=FFontPushedState.Style;
//  Font:=FFontPushedState;
//  Font.Color:=FFontColorPushedState;
  end;
end;

procedure TFlatButton.WndProc(var message: TMessage);
var P1,P2 : TPoint;
begin
if (Parent <> nil) and FEnableButton then
  begin
  GetCursorPos(P1);
  P2 := Self.ScreenToClient(P1);
  if not((P2.X > 0) and (P2.X < Width-1) and (P2.Y > 0) and (P2.Y < Height-1))
    then MouseUnOver;
  end;
inherited WndProc(message);
end;

procedure TFlatButton.SetBtnType (Value: TBtnType);
begin
Color:=FColorNormalState;
Font:=FFontNormalState;
//Font.Color:=FFontColorNormalState;
FBtnType:= Value;
case Value of
  Border01:
    begin
    BevelInner:= bvNone;
    BevelOuter:= bvNone;
    Ctl3D:= False;
    BorderStyle:= bsSingle;
    end;
  Border02:
    begin
    BevelInner:= bvLowered;
    BevelOuter:= bvRaised;
    Ctl3D:= True;
    BorderStyle:= bsNone;
    end;
  Bevel:
    begin
    BevelInner:= bvNone;
    BevelOuter:= bvRaised;
    Ctl3D:= True;
    BorderStyle:= bsNone;
    end;
  Flat:
    begin
    BevelInner:= bvNone;
    BevelOuter:= bvNone;
    Ctl3D:= True;
    BorderStyle:= bsNone;
    end;
  end;
end;

procedure TFlatButton.SetColorHighLight(Value: TColor);
begin
FColorHighLight:=Value;
end;

procedure TFlatButton.SetColorNormalState(Value: TColor);
begin
FColorNormalState:=Value;
Color:=FColorNormalState;
end;

procedure TFlatButton.SetColorPushedState(Value: TColor);
begin
FColorPushedState:=Value;
end;

procedure TFlatButton.SetColorDisabled(Value: TColor);
begin
FColorDisabled:=Value;
end;

procedure TFlatButton.SetFontHighLight(Value: TFormalFont);
begin
//FFontHighLight:=Value;
FFontHighLight.Charset:=Value.Charset;
FFontHighLight.Color:=Value.Color;
FFontHighLight.Height:=Value.Height;
FFontHighLight.Name:=Value.Name;
FFontHighLight.Pitch:=Value.Pitch;
FFontHighLight.Size:=Value.Size;
FFontHighLight.Style:=Value.Style;
end;

procedure TFlatButton.SetFontNormalState(Value: TFont);
begin
FFontNormalState:=Value;
//Font:=FFontNormalState;
end;

procedure TFlatButton.SetFontPushedState(Value: TFormalFont);
begin
//FFontPushedState:=Value;
FFontPushedState.Charset:=Value.Charset;
FFontPushedState.Color:=Value.Color;
FFontPushedState.Height:=Value.Height;
FFontPushedState.Name:=Value.Name;
FFontPushedState.Pitch:=Value.Pitch;
FFontPushedState.Size:=Value.Size;
FFontPushedState.Style:=Value.Style;
end;

procedure TFlatButton.SetFontDisabled(Value: TFormalFont);
begin
//FFontDisabled:=Value;
FFontDisabled.Charset:=Value.Charset;
FFontDisabled.Color:=Value.Color;
FFontDisabled.Height:=Value.Height;
FFontDisabled.Name:=Value.Name;
FFontDisabled.Pitch:=Value.Pitch;
FFontDisabled.Size:=Value.Size;
FFontDisabled.Style:=Value.Style;
end;

procedure TFlatButton.SetHold (Value: boolean);
begin
if not value then
  begin
  SetDown(false);
  end;
FHold:=Value;
end;

procedure TFlatButton.SetDown(Value: boolean);
var OldVal: boolean;
begin
if FHold then
  begin

Oldval:=FDown;
FDown:=Value;

if value then

  begin
  Color:=FColorPushedState;
  Font.Charset:=FFontPushedState.Charset;
  Font.Color:=FFontPushedState.Color;
  Font.Height:=FFontPushedState.Height;
  Font.Name:=FFontPushedState.Name;
  Font.Pitch:=FFontPushedState.Pitch;
  Font.Size:=FFontPushedState.Size;
  Font.Style:=FFontPushedState.Style;
//  Font:=FFontPushedState;
//  Font.Color:=FFontColorPushedState;
  case FBtnType of
    Border01:
      begin
      BevelInner:= bvNone;
      BevelOuter:= bvNone;
      Ctl3D:= False;
      BorderStyle:= bsSingle;
      end;
    Border02:
      begin
      BevelInner:=bvLowered;
      BevelOuter:=bvLowered;
      Ctl3D:= True;
      BorderStyle:= bsNone;
      end;
    Bevel:
      begin
      BevelInner:=bvNone;
      BevelOuter:=bvLowered;
      Ctl3D:= True;
      BorderStyle:= bsNone;
      end;
    Flat:
      begin
      BevelInner:= bvNone;
      BevelOuter:= bvNone;
      Ctl3D:= True;
      BorderStyle:= bsNone;
      end;
    end;
  if FMoveOnClick and OldVal<>FDown then
    begin
    Left:=Left+1;
    Top:=Top+1;
    end;
  FCanGo:=true;
  end else
  begin

  Color:=FColorNormalState;
  Font:=FFontNormalState;
//  Font.Color:=FFontColorNormalState;
  case FBtnType of
    Border01:
      begin
      BevelInner:= bvNone;
      BevelOuter:= bvNone;
      Ctl3D:= False;
      BorderStyle:= bsSingle;
      end;
    Border02:
      begin
      BevelInner:= bvLowered;
      BevelOuter:= bvRaised;
      Ctl3D:= True;
      BorderStyle:= bsNone;
      end;
    Bevel:
      begin
      BevelInner:= bvNone;
      BevelOuter:= bvRaised;
      Ctl3D:= True;
      BorderStyle:= bsNone;
      end;
    Flat:
      begin
      BevelInner:= bvNone;
      BevelOuter:= bvNone;
      Ctl3D:= True;
      BorderStyle:= bsNone;
      end;
    end;
  if FMoveOnClick and OldVal<>FDown then
    begin
    Left:=Left-1;
    Top:=Top-1;
    end;
  end;

  end;
end;

procedure TFlatButton.SetEnableButton(Value: boolean);
begin
FEnableButton:=Value;
Enabled:=Value;
if Value then
  begin
  Color:=FColorNormalState;
//  Font.Color:=FFontColorNormalState;
  Font:=FFontNormalState;
  end else
  begin
  Color:=FColorDisabled;
  Font.Charset:=FFontDisabled.Charset;
  Font.Color:=FFontDisabled.Color;
  Font.Height:=FFontDisabled.Height;
  Font.Name:=FFontDisabled.Name;
  Font.Pitch:=FFontDisabled.Pitch;
  Font.Size:=FFontDisabled.Size;
  Font.Style:=FFontDisabled.Style;
//  Font.Color:=FFontColorDisabled;
//  Font:=FFontDisabled;
  end;
end;


end.
